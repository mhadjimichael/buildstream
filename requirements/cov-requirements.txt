coverage==4.4
pytest-cov==2.6.1
## The following requirements were added by pip freeze:
atomicwrites==1.3.0
attrs==18.2.0
more-itertools==6.0.0
pathlib2==2.3.3
pluggy==0.9.0
py==1.8.0
pytest==4.3.0
six==1.12.0
